<?php
function connect_database() {
  //konfigurasi database
  $db_user = "gn15a7";
  $db_password = "gTUtvtDgHmHe";
  $db_name = "gn15a7";
  $db_host = "localhost";
  
  //akan selalu dilakukan, lebih baik jika ditulis dalam sebuah fungsi tersendiri
  $koneksi = mysqli_connect($db_host, $db_user, $db_password, $db_name);
  return $koneksi;
}

function cek_pass($id, $oldpass){
  $koneksi = connect_database();
  $sql = "SELECT password FROM userlogin WHERE id = ?";
  $stmt = mysqli_prepare($koneksi, $sql);
  mysqli_stmt_bind_param($stmt, "i", $id);
  mysqli_stmt_execute($stmt);
  mysqli_stmt_store_result($stmt);
  $cek = array();
  mysqli_stmt_bind_result($stmt, $password);
  mysqli_stmt_fetch($stmt);

  $cek = array("password" => $password);
  mysqli_stmt_close($stmt);
  mysqli_close($koneksi);
  return $cek;
}

function get_edit($id) {
  $koneksi = connect_database();
  $sql = "SELECT nama, nim, birth_place, birth_date, gender, hometown, email, quotes, foto, username FROM userlogin WHERE id = ?";
  $stmt = mysqli_prepare($koneksi, $sql);
  mysqli_stmt_bind_param($stmt, "i", $id);
  mysqli_stmt_execute($stmt);
  mysqli_stmt_store_result($stmt);
  $userz = array();
  if(mysqli_stmt_num_rows($stmt)) {
    mysqli_stmt_bind_result($stmt, $nama, $nim, $birth_place, $birth_date, $gender, $hometown, $email, $quotes, $foto, $username);
    mysqli_stmt_fetch($stmt);

    //ubah ke bentuk array
    $userz = array("id" => $id, "nama" => $nama, "nim" => $nim, "birth_place" => $birth_place, "birth_date" => $birth_date, "gender" => $gender, "hometown" => $hometown, "email" => $email, "quotes" => $quotes, "foto" => $foto, "username" => $username);
  }
  mysqli_stmt_close($stmt);
  mysqli_close($koneksi);
  return $userz;
}

function get_produk($no){
  $koneksi = connect_database();
  $sql = "select username, nama, no, userlogin.id, judul, deskripsi, time, produk.foto, votes from produk INNER JOIN userlogin ON userlogin.id=produk.id AND no='$no'";
  $stmt = mysqli_prepare($koneksi, $sql);
  mysqli_stmt_execute($stmt);
  mysqli_stmt_store_result($stmt);
  $userz = array();
  if(mysqli_stmt_num_rows($stmt)) {
    mysqli_stmt_bind_result($stmt, $username, $nama, $no, $id, $judul, $deskripsi, $time, $foto, $votes);
    mysqli_stmt_fetch($stmt);

    //ubah ke bentuk array
    $userz = array("username" => $username, "nama" => $nama, "no" => $no, "id" => $id, "judul" => $judul, "deskripsi" => $deskripsi, "time" => $time, "foto" => $foto, "votes" => $votes);
  }
  mysqli_stmt_close($stmt);
  mysqli_close($koneksi);
  return $userz;
}

function get_req($no){
  $koneksi = connect_database();
  $sql = "select username, nama, no, userlogin.id, judul, deskripsi, time, req.foto from req INNER JOIN userlogin ON userlogin.id=req.id AND no='$no'";
  $stmt = mysqli_prepare($koneksi, $sql);
  mysqli_stmt_execute($stmt);
  mysqli_stmt_store_result($stmt);
  $userz = array();
  if(mysqli_stmt_num_rows($stmt)) {
    mysqli_stmt_bind_result($stmt, $username, $nama, $no, $id, $judul, $deskripsi, $time, $foto);
    mysqli_stmt_fetch($stmt);

    //ubah ke bentuk array
    $userz = array("username" => $username, "nama" => $nama, "no" => $no, "id" => $id, "judul" => $judul, "deskripsi" => $deskripsi, "time" => $time, "foto" => $foto);
  }
  mysqli_stmt_close($stmt);
  mysqli_close($koneksi);
  return $userz;
}

function update_user2($id, $nama, $nim, $birth_place, $birth_date, $gender, $hometown, $email, $quotes) {
  $koneksi = connect_database();
  
  //escape input
  $nama = mysqli_real_escape_string($koneksi, $nama);
  $nim = mysqli_real_escape_string($koneksi, $nim);
  $birth_place = mysqli_real_escape_string($koneksi, $birth_place);
  $birth_date = mysqli_real_escape_string($koneksi, $birth_date);
  $gender = mysqli_real_escape_string($koneksi, $gender);
  $hometown = mysqli_real_escape_string($koneksi, $hometown);
  $email = mysqli_real_escape_string($koneksi, $email);
  $quotes = mysqli_real_escape_string($koneksi, $quotes);

  $sql = "UPDATE userlogin SET nama = ?, nim = ?, birth_place = ?, birth_date = ?, gender = ?, hometown = ?, email = ?, quotes = ? WHERE id = ?";
  $stmt = mysqli_prepare($koneksi, $sql);
  mysqli_stmt_bind_param($stmt, "sissssssi", $nama, $nim, $birth_place, $birth_date, $gender, $hometown, $email, $quotes, $id);
  mysqli_stmt_execute($stmt);
  mysqli_stmt_close($stmt);
  mysqli_close($koneksi);
}

function update_user($id, $nama, $birth_place, $birth_date, $gender, $hometown, $email, $quotes) {
  $koneksi = connect_database();
  
  //escape input
  $nama = mysqli_real_escape_string($koneksi, $nama);
  $birth_place = mysqli_real_escape_string($koneksi, $birth_place);
  $birth_date = mysqli_real_escape_string($koneksi, $birth_date);
  $gender = mysqli_real_escape_string($koneksi, $gender);
  $hometown = mysqli_real_escape_string($koneksi, $hometown);
  $email = mysqli_real_escape_string($koneksi, $email);
  $quotes = mysqli_real_escape_string($koneksi, $quotes);

  $sql = "UPDATE userlogin SET nama = ?, birth_place = ?, birth_date = ?, gender = ?, hometown = ?, email = ?, quotes = ? WHERE id = ?";
  $stmt = mysqli_prepare($koneksi, $sql);
  mysqli_stmt_bind_param($stmt, "sssssssi", $nama, $birth_place, $birth_date, $gender, $hometown, $email, $quotes, $id);
  mysqli_stmt_execute($stmt);
  mysqli_stmt_close($stmt);
  mysqli_close($koneksi);
}

function upload_user($id, $foto){
  $koneksi = connect_database();
  $sql = "UPDATE userlogin SET foto = ? WHERE id = ?";
  $stmt = mysqli_prepare($koneksi, $sql);
  mysqli_stmt_bind_param($stmt, "si", $foto, $id);
  mysqli_stmt_execute($stmt);
  mysqli_stmt_close($stmt);
  mysqli_close($koneksi);
}

function update_pass($id, $oldpass, $newpass) {
  $koneksi = connect_database();
  
   
    //escape input
  $newpass = mysqli_real_escape_string($koneksi, $newpass);

  $sql = "UPDATE userlogin SET password = ? WHERE id = ?";
  $stmt = mysqli_prepare($koneksi, $sql);
  mysqli_stmt_bind_param($stmt, "si", $newpass, $id);
  mysqli_stmt_execute($stmt);
  mysqli_stmt_close($stmt);
  mysqli_close($koneksi);
}

function delete_user($id) {
  $koneksi = connect_database();
  $row = mysqli_query($koneksi,"select * from userlogin where id='$id'");
    $a = mysqli_fetch_array($row,MYSQLI_BOTH);
  unlink($a['foto']);
  $sql = "DELETE FROM userlogin WHERE id = ?";
  $stmt = mysqli_prepare($koneksi, $sql);
  mysqli_stmt_bind_param($stmt, "i", $id);
  mysqli_stmt_execute($stmt);
  mysqli_stmt_close($stmt);
  mysqli_close($koneksi);
}

function delete_post($no) {
  $koneksi = connect_database();
  $row = mysqli_query($koneksi,"select * from produk where no='$no'");
    $a = mysqli_fetch_array($row,MYSQLI_BOTH);
  unlink($a['foto']);
  $sql = "DELETE FROM produk WHERE no = ?";
  $stmt = mysqli_prepare($koneksi, $sql);
  mysqli_stmt_bind_param($stmt, "i", $no);
  mysqli_stmt_execute($stmt);
  mysqli_stmt_close($stmt);
  mysqli_close($koneksi);
}


function add_product($id, $judul, $deskripsi, $waktu) {
  $koneksi = connect_database();

  //escape input
  $judul = mysqli_real_escape_string($koneksi, $judul);
  $deskripsi = mysqli_real_escape_string($koneksi, $deskripsi);
  $id = mysqli_real_escape_string($koneksi, $id);
  

  $sql = "INSERT INTO req (id, judul, deskripsi, time) VALUES (?, ?, ?, ?)";
  $stmt = mysqli_prepare($koneksi, $sql);
  mysqli_stmt_bind_param($stmt, "isss", $id, $judul, $deskripsi, $waktu);
  mysqli_stmt_execute($stmt);
  $newno = mysqli_insert_id($koneksi);
  mysqli_stmt_close($stmt);
  mysqli_close($koneksi);
  return $newno;
}

function update_post($no, $id, $judul, $deskripsi, $time){
  $koneksi = connect_database();
    //escape input
  $judul = mysqli_real_escape_string($koneksi, $judul);
  $deskripsi = mysqli_real_escape_string($koneksi, $deskripsi);

  $r = mysqli_query($koneksi,"select * from req where no_edit = '$no'");
    while($x = mysqli_fetch_array($r, MYSQL_ASSOC)) {
    }

  if(mysqli_num_rows($r) == 0){
    $sql = "INSERT INTO req (id, judul, deskripsi, time, no_edit) VALUES (?,?,?,?,?)";
    $stmt = mysqli_prepare($koneksi, $sql);
  mysqli_stmt_bind_param($stmt, "isssi", $id, $judul, $deskripsi, $time, $no);
  mysqli_stmt_execute($stmt);
  $newno = mysqli_insert_id($koneksi);
  }
  else{
  $sql = "UPDATE req SET judul = ?, deskripsi = ? WHERE no_edit = ?";
  $stmt = mysqli_prepare($koneksi, $sql);
  mysqli_stmt_bind_param($stmt, "ssi", $judul, $deskripsi, $no);
  mysqli_stmt_execute($stmt);
  
  $cek = mysqli_query($koneksi,"select no from req where no_edit = '$no'");
  $t = mysqli_fetch_array($cek, MYSQL_ASSOC);
  $newno= $t['no'];
  }
  
  mysqli_stmt_close($stmt);
  mysqli_close($koneksi);
  return $newno;
  }

  function upload_req($newno, $foto){
  $koneksi = connect_database();
  $sql = "UPDATE req SET foto = ? WHERE no = ?";
  $stmt = mysqli_prepare($koneksi, $sql);
  mysqli_stmt_bind_param($stmt, "si", $foto, $newno);
  mysqli_stmt_execute($stmt);
  mysqli_stmt_close($stmt);
  mysqli_close($koneksi);
}

  function upload_show($newno, $foto){
  $koneksi = connect_database();
  $sql = "UPDATE produk SET foto = ? WHERE no = ?";
  $stmt = mysqli_prepare($koneksi, $sql);
  mysqli_stmt_bind_param($stmt, "si", $foto, $newno);
  mysqli_stmt_execute($stmt);
  mysqli_stmt_close($stmt);
  mysqli_close($koneksi);
}

function delete_req($no) {
  $koneksi = connect_database();
  $row = mysqli_query($koneksi,"select * from req where no='$no'");
    $a = mysqli_fetch_array($row,MYSQLI_BOTH);
  unlink($a['foto']);
  $sql = "DELETE FROM req WHERE no = ?";
  $stmt = mysqli_prepare($koneksi, $sql);
  mysqli_stmt_bind_param($stmt, "i", $no);
  mysqli_stmt_execute($stmt);
  mysqli_stmt_close($stmt);
  mysqli_close($koneksi);
}

function approve_req($no) {
  $koneksi = connect_database();
  $row = mysqli_query($koneksi,"select * from req where no='$no'");
  $a = mysqli_fetch_array($row,MYSQLI_BOTH);

  if($a['no_edit'] == ""){
    $sql = "INSERT INTO produk (id, judul, deskripsi, time) VALUES (?, ?, ?, ?)";
    $stmt = mysqli_prepare($koneksi, $sql);
    mysqli_stmt_bind_param($stmt, "isss", $a['id'], $a['judul'], $a['deskripsi'], $a['time']);
    mysqli_stmt_execute($stmt);
    $newno = mysqli_insert_id($koneksi);
  }
  else{
    $sql = "UPDATE produk SET judul = ?, deskripsi = ?, time = ? WHERE no = ?";
    $stmt = mysqli_prepare($koneksi, $sql);
    mysqli_stmt_bind_param($stmt, "sssi", $a['judul'], $a['deskripsi'], $a['time'], $a['no_edit']);
    mysqli_stmt_execute($stmt);
    $newno= $a['no_edit'];
  }
  mysqli_stmt_close($stmt);
  mysqli_close($koneksi);
  return $newno;
}

function change_vote($vote, $no){
  $koneksi = connect_database();
  $sql = "UPDATE produk SET votes = ? WHERE no = ?";
  $stmt = mysqli_prepare($koneksi,$sql);
  mysqli_stmt_bind_param($stmt, "ii", $vote, $no);
  mysqli_stmt_execute($stmt);
}

function del_vote($no, $id){
  $koneksi = connect_database();
  $sql = "DELETE FROM vote WHERE no_produk = '$no' AND id_user = '$id'";
  mysqli_query($koneksi, $sql);
}

function ins_vote($no, $id){
  $koneksi = connect_database();
  $sql = "INSERT INTO vote (no_produk, id_user) VALUES (?, ?)";
            $stmt = mysqli_prepare($koneksi, $sql);
            mysqli_stmt_bind_param($stmt, "ii",$no, $id);
            mysqli_stmt_execute($stmt);
             mysqli_insert_id($koneksi);
}
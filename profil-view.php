<?php
  //ambil id yang akan diedit
  if(isset($_GET['id'])) {
    $id = $_GET['id'];
    //ambil quote dengan id tersebut
    require_once("database.php");
    $userz = get_edit($id);

    //cek apakah ada quote dengan id tersebut?
    if(!count($userz)) {
      header("Location: index.php");
    }
  }
  else {
    header("Location: index.php");
  }   
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Duta Wacana Showchase | Show Your Product Here!</title>
		<link type="text/css" rel="stylesheet" href="css/stylesheet.css"/>
    <meta charset="UTF-8"/>
    <script type="text/javascript" src="js/javascript.js"></script>
	</head>

	<body>
  
		<!--HEADER-->
    <div class="container top">
      <div class="logo">
                <img src="img/logo.png" alt="logo"/>
      </div>
      <div class="judul-kanan judul-lebar">
                <h1>DUTA WACANA SHOWCASE</h1>
                <p>SHOW OFF YOUR PRODUCT! </p>
      </div>
    </div>
    
    
   <!--NAVIGASI BAR--> 
   <?php 

   $con = mysqli_connect("localhost","gn15a7", "gTUtvtDgHmHe");
    mysqli_select_db($con,"gn15a7");
    
    $r = mysqli_query($con,"select * from req");
    while($x = mysqli_fetch_array($r, MYSQL_ASSOC)) {
      $req[] = $x;
    }
    
    
    
    session_start();
    if(isset($_SESSION["UNAME"]) && $_SESSION["UNAME"] != ""){
      $user = $_SESSION["UNAME"];
      $row = mysqli_query($con,"select * from userlogin where username='$user'");
      $a = mysqli_fetch_array($row,MYSQLI_BOTH);

      if($_SESSION["UNAME"]==$userz['username']){
          header("Location: profil.php");
      }
     
      echo ' <div class="nav-atas shadow-nav text-nav" id="nav-top">
            <div class="isi-nav-kiri">
                  <a href="index.php" class="topnav-icons-icon-left selected"
                  title="Home">Home</a>
                  <a href="product.php" class="topnav-icons-icon-left"
                  title="product">My Product</a>
                  <a href="aboutUs.php" class="topnav-icons-icon-left">About Us</a>
                  <a href="history.php" class="topnav-icons-icon-left"
                  title="History">History</a>';
                  if($_SESSION["UNAME"] == "admin"){
                    echo '<a href="list.php" class="topnav-icons-icon-left"
                  title="List User">List User</a>
                  <a href="listpost.php" class="topnav-icons-icon-left"
                  title="List Post">List Post</a>';
                  if(mysqli_num_rows($r) == 0){
                  echo'
                  <a href="reqpost.php" class="topnav-icons-icon-left"
                  title="List Post">Req Post</a>';
                  }
                  else{
                    echo'
                  <a href="reqpost.php" class="topnav-icons-icon-left red-alert"
                  title="List Post">Req Post ('. count($req) .')</a>';
                  }
                    }
         echo '   
            </div>
            <div class="dropdown button">
                <a onclick="myFunction()" class="dropbtn" title="Profile">Welcome, '.$_SESSION['UNAME'].'!</a>
                <div id="myDropdown" class="dropdown-content">
                  <a href="profil.php">Profile</a>
                  <a href="edit.php?id='.$a['id'].'">Edit Profile</a>
                  <a href="editpass.php?id='.$a['id'].'">Change Password</a>

                  <a href="add_Product.php">Add Product</a>
                  <a href="logout.php">Logout</a>
                </div>
              </div>
    </div>';
    }
    else{
      echo'<div class="nav-atas shadow-nav text-nav" id="nav-top">
        <div style="overflow: auto;">
            <div class="isi-nav-kiri">
                  <a href="index.php" class="topnav-icons-icon-left selected"
                  title="Home">Home</a>
                  <a href="aboutUs.php" class="topnav-icons-icon-left">About Us</a>
                  
            </div>
        </div>
        <a href="login.php" class="topnav-icons-icon-left login-nav"
                  title="Login">Login</a>
    </div>';
    }
    ?>
    
		<div class="slidebar">.</div>

    <div class="profile-size">
      <div class="profile-autosize">
        <?php 
          if ($userz["foto"] == "") {
            echo '<img src="img/default.jpg" alt="image" />';
          } else {
          echo '<img src="'. $userz['foto'] . '" alt="image" />';
          }
            ?>
        
      </div>
      <div class="profile-name">
        <p>Profile</p>
        <br>
        <p><?php echo $userz['nama'] ?></p>
        <hr>
        <span><?php echo $userz['nim'] ?></span>
      </div>
    </div>

    <hr>
    <div class="profile-about">
        <div id="aboutme"><h1>About <?php echo $userz['nama'] ?></h1></div>

        <table>
          <tr>
            <td>Name</td>
            <td>: <?php echo $userz['nama'] ?></td>
          </tr>        
          <tr>
            <td>Birth</td>
            <td>: <?php echo $userz['birth_place'] ?>, <?php echo $userz['birth_date'] ?></td>
          </tr>
          <tr>
            <td>Gender</td>
            <td>: <?php echo $userz['gender'] ?></td>
          </tr>
          <tr>
            <td>Hometown</td>
            <td>: <?php echo $userz['hometown'] ?></td>
          </tr>
          <tr>
            <td>Email</td>
            <td>: <?php echo $userz['email'] ?></td>
          </tr>
          <tr>
            <td>Quotes</td>
            <td>: <?php echo $userz['quotes'] ?></td>
          </tr>
        </table>
    </div>
    <hr>
  
	</body>
</html>
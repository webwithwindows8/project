-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 13, 2016 at 03:37 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gn15a7`
--

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `no` int(5) NOT NULL,
  `id` int(5) NOT NULL,
  `judul` varchar(20) NOT NULL,
  `deskripsi` varchar(300) NOT NULL,
  `time` date NOT NULL,
  `foto` varchar(225) NOT NULL,
  `votes` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`no`, `id`, `judul`, `deskripsi`, `time`, `foto`, `votes`) VALUES
(1, 1, 'Tes', 'Testing testing', '2016-05-05', 'img/show/show-1.jpg', 0),
(2, 2, 'Hello', 'Hello', '2016-05-03', 'img/show/show-2.gif', 0),
(3, 1, 'Unknown Product', 'The Picture still unknown', '2016-05-06', '', 0),
(9, 1, 'Tes doang 3', 'asddask', '2016-05-06', 'img/show/show-9.jpg', 0),
(10, 1, 'IM ADMIN', 'asd', '2016-05-06', 'img/show/show-10.jpg', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `no` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
    if(isset($_POST['id']) && isset($_POST['nama']) && isset($_POST['nim']) && isset($_POST['birth_place']) && isset($_POST['birth_date']) && isset($_POST['gender']) && isset($_POST['hometown']) && isset($_POST['email']) && isset($_POST['quotes'])) {
        $id = $_POST['id'];
        $nama = $_POST['nama'];
        $nim = $_POST['nim'];
        $birth_place = $_POST['birth_place'];
        $birth_date = $_POST['birth_date'];
        $gender = $_POST['gender'];
        $hometown = $_POST['hometown'];
        $email = $_POST['email'];
        $quotes = $_POST['quotes'];
        require_once("database.php");
        update_user2($id, $nama, $nim, $birth_place, $birth_date, $gender, $hometown, $email, $quotes);
    }
    header("Location: list.php");
    
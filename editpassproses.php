<?php
if(isset($_POST['id']) && isset($_POST['newpass']) && isset($_POST['newpass2'])) {
    $id = $_POST['id'];
    $oldpass = $_POST['oldpass'];
    $newpass = $_POST['newpass'];
    $newpass2 = $_POST['newpass2'];

    if($newpass == $newpass2){
        require_once("database.php");
        $cek = cek_pass($id, $oldpass);
        if($cek['password'] == $oldpass){
            $id = $_POST['id'];
            $oldpass = $_POST['oldpass'];
            $newpass = $_POST['newpass'];
            require_once("database.php");
            update_pass($id, $oldpass, $newpass);
            
            $message = "Password Change Successful";
            echo "<SCRIPT type='text/javascript'>
            alert('$message');
            window.location.replace(\"profil.php\");
            </SCRIPT>";
            mysql_close();
        }
        else{
            $message = "Old Password incorrect";
            echo "<SCRIPT type='text/javascript'>
            alert('$message');
            window.location.replace(\"editpass.php?id=$id\");
            </SCRIPT>";
            mysql_close();
        }
    }
    else{
        $message = "Re-type Password do not match";
        echo "<SCRIPT type='text/javascript'>
        alert('$message');
        window.location.replace(\"editpass.php?id=$id\");
        </SCRIPT>";
        mysql_close();
    }
}
else{
    header("Location: login.php");
}
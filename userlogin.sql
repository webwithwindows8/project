-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2016 at 09:59 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gn15a7`
--

-- --------------------------------------------------------

--
-- Table structure for table `userlogin`
--

CREATE TABLE `userlogin` (
  `id` int(5) NOT NULL,
  `nim` int(8) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `nama` varchar(50) NOT NULL,
  `birth_place` varchar(30) NOT NULL,
  `birth_date` varchar(20) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `hometown` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `quotes` varchar(200) NOT NULL,
  `foto` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userlogin`
--

INSERT INTO `userlogin` (`id`, `nim`, `username`, `password`, `nama`, `birth_place`, `birth_date`, `gender`, `hometown`, `email`, `quotes`, `foto`) VALUES
(1, 0, 'admin', 'admin', 'adminz', 'Unknown', '00-00-0000', 'Male', 'Unknown', 'Unknown', 'Unknown', 'img/profil/profil-1.gif'),
(2, 7140000, 'informatika', 'informatika', 'informatika', '0', '00-00-00', '0', '0', 'informatika@ti.ukdw.id', 'kunci kesuksesan adalah key of success', 'img/profil/profil-2.gif'),
(3, 71140000, '71140000', 'johncena', 'John Cena', 'Yogyakarta', '19-09-1990', 'Man', 'Sleman', 'johncenaganteng@ti.ukdw.ac.id', '"To succeed, I have to believe everynight in my heart, that I am the best"', 'img/profil/profil-3.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `userlogin`
--
ALTER TABLE `userlogin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `userlogin`
--
ALTER TABLE `userlogin`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

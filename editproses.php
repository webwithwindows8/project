<?php
    if(isset($_POST['id']) && isset($_POST['nama']) && isset($_POST['birth_place']) && isset($_POST['birth_date']) && isset($_POST['gender']) && isset($_POST['hometown']) && isset($_POST['email']) && isset($_POST['quotes'])) {
        $id = $_POST['id'];
        $nama = $_POST['nama'];
        $birth_place = $_POST['birth_place'];
        $birth_date = $_POST['birth_date'];
        $gender = $_POST['gender'];
        $hometown = $_POST['hometown'];
        $email = $_POST['email'];
        $quotes = $_POST['quotes'];
        $foto = $_FILES["foto"];

        require_once("database.php");
        update_user($id, $nama, $birth_place, $birth_date, $gender, $hometown, $email, $quotes);

        $check = getimagesize($foto["tmp_name"]);

        if($check !== false) {
            $ext = pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION);
            $newfilename="profil-".$id .".".$ext;
            $uploadfile = 'img/profil/'.$newfilename;
            $foto = $uploadfile;
            foreach(glob("./img/profil/profil-$id.{jpg,jpeg,png,gif}", GLOB_BRACE) as $filedell){
                unlink($filedell);
            }
            if(move_uploaded_file($_FILES["foto"]["tmp_name"], $uploadfile)){
                require_once("database.php");
                upload_user($id,$foto);
            }
            else{
                $message = "Upload picture failed";
                echo "<SCRIPT type='text/javascript'>
                alert('$message');
                window.location.replace(\"profil.php\");
                </SCRIPT>";
                mysql_close();
            }
        }
        
    }
 header("Location: profil.php");
    
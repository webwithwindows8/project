<?php 
session_start();
if(isset($_SESSION["UNAME"]) && $_SESSION["UNAME"] != ""){
	header("Location: profil.php");
}
?>
<!DOCTYPE html>
<html>
  <head>
  	<title>Duta Wacana Showchase | Show Your Product Here!</title>
		<link type="text/css" rel="stylesheet" href="css/stylesheet.css"/>
    <meta charset="UTF-8"/>
    <script type="text/javascript" src="js/javascript.js"></script>
	</head>

	<body>
  
  <!--HEADER-->
		<div class="container top">
			<div class="logo">
                <img src="img/logo.png" alt="logo"/>
      </div>
			<div class="judul-kanan judul-lebar">
                <h1>DUTA WACANA SHOWCASE</h1>
  			        <p>SHOW OFF YOUR PRODUCT! </p>
      </div>
    </div>
    
    
    
   <!--NAVIGASI BAR-->
    <div class="nav-atas shadow-nav text-nav" id="nav-top">
            <div class="isi-nav-kiri">
              <a href="index.php" class="topnav-icons-icon-left"
                  title="Home">Home</a>
                  <a href="aboutUs.php" class="topnav-icons-icon-left"
                  title="About Us">About Us</a>             
            </div>
        <a href="login.php" class="selected login-nav">Login</a>
    </div>
    
    
    <!--KOTAK LOGIN-->
		<div class="login-kotak">
          <div class="login-text">
              Log In Here
          </div>
          
          <div class="user-pass">
            <form action="loginproses.php" method="post">
              <pre><input type="text" id="login_text" name="username" placeholder="Username" value=""></pre>
              <pre><input type="password" name="password" placeholder="Password" value=""></pre>
              <input type="submit" onclick="login();" value="Log In" class="login-box">
              
            </form>
            <a href="forgotpas.html">forgot your password?</a>
          </div>
    </div>
    
        <div class="slidebar">.</div>
	</body>
</html>
